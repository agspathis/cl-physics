(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :cl-slice))
;; (defpackage :physics
;;   (:use :common-lisp :cl-slice))
;; (in-package :physics)

(setq *print-case* :downcase)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; state
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass state ()
  ((tn
    :initform (error "Time (:time) not provided.")
    :initarg :time
    :accessor tn
    :documentation "The time instance.")
   (q
    :initform (error "Q (:q) not provided.")
    :initarg :q
    :accessor q
    :documentation "The position variable of the state.")
   (u
    :initform (error "U (:u) not provided.")
    :initarg :u
    :accessor u
    :documentation "The velocity variable of the state.")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; system
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass system ()
  ((last-q-index
    :initform 0
    :accessor last-q-index
    :documentation "The index of the next position variable in the state.")
   (last-u-index
    :initform 0
    :accessor last-u-index
    :documentation "The index of the next velocity variable in the state.")
   (components
    :initform nil
    :accessor components
    :documentation "List of components added in this system.")))

(defgeneric add-to-system (system component)
  (:documentation "Adds a component to the system."))

(defmethod initialize-system ((system system))
  "Initializes the system and returns the state."
  (with-slots (last-q-index last-u-index)
    system
    (make-instance 'state
                   :time 0
                   :q (make-array last-q-index :initial-element 'nil)
                   :u (make-array last-u-index :initial-element 'nil))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; component
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass component ()
  ((nq
    :initform (error "Number of position variables not specified.")
    :accessor nq
    :documentation "Dimension of position variables.")
   (nu
    :initform (error "Number of velocity variables not specified.")
    :accessor nu
    :documentation "Dimension of velocity variables.")
   (q-index
    :initform nil
    :accessor q-index
    :documentation "The index of the position variable in the state.")
   (u-index
    :initform nil
    :accessor u-index
    :documentation "The index of the velocity variable in the state.")))

(defmethod add-to-system ((system system) (component component))
  (with-slots (components last-q-index last-u-index)
      system
    (assert (not (member component components))
            (component)
            "Component ~S is already present in the system" component)
    (setf components (append components (list component)))
    (with-slots (nq q-index nu u-index)
        component
      (setf q-index last-q-index)
      (incf last-q-index nq)
      (setf u-index last-u-index)
      (incf last-u-index nu))))

(defmethod set-q ((state state) (component component) values)
  "Sets the position state variable of the component from values."
  (with-slots (nq q-index)
      component
    (assert (= (length values) nq)
            (values nq)
            "Dimensions mismatch ~S != ~S" (length values) nq)
    (with-slots (q)
        state
      (loop for i from 0 below nq
            do (setf (aref q (+ q-index i)) (aref values i))))))

(defmethod set-u ((state state) (component component) values)
  "Sets the velocity state variable of the component from values."
  (with-slots (nu u-index)
      component
    (assert (= (length values) nu)
            (values nu)
            "Dimensions mismatch ~S != ~S" (length values) nu)
    (with-slots (u)
        state
      (loop for i from 0 below nu
            do (setf (aref u (+ u-index i)) (aref values i))))))

(defmethod get-q ((state state) (component component))
  "Gets the position state variable of the component."
  (with-slots (nq q-index)
      component
    (with-slots (q)
        state
      (cl-slice:slice q (cl-slice:including q-index (+ q-index nq -1))))))

(defmethod get-u ((state state) (component component))
  "Gets the velocity state variable of the component."
  (with-slots (nu u-index)
      component
    (with-slots (u)
        state
      (cl-slice:slice u (cl-slice:including u-index (+ u-index nu -1))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; particle
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass particle (component)
  ((nq
    :initform 3)
   (nu
    :initform 3)
   (mass
    :initform (error "Particle mass not specified.")
    :initarg :mass
    :accessor mass
    :documentation "The mass of the particle.")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; problem definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconstant +au+ 1.496e8
  "AU (Astronomic Unit) 1 AU is the distance between sun and earth.")
(defconstant +sun-mass+ 1.9885e30 "The mass of the sun in kg.")
(defconstant +earth-mass+ 5.972e24 "The mass of the earth in kg.")

(defparameter *system* (make-instance 'system))
(defparameter *sun* (make-instance 'particle :mass +sun-mass+))
(defparameter *earth* (make-instance 'particle :mass +earth-mass+))

(add-to-system *system* *sun*)
(add-to-system *system* *earth*)
(defparameter *state* (initialize-system *system*))
(set-q *state* *sun* #(0 0 0))
(set-u *state* *sun* #(0 0 0))
(set-q *state* *earth* #(+au+ 0 0))
(set-u *state* *earth* #(0 35000.0 0))
